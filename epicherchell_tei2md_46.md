---
title : "vol 1 - Épitaphe d'une anonyme, faite par Iulia Secunda"
author : ""
date : ""
publisher : "Epicherchell Corpus des inscriptions antiques de Cherchell - Centre Camille Jullian"
rights : "Licensed under a Creative Commons CC BY-NC-ND 4.0."
---

# vol 1 - Épitaphe d'une anonyme, faite par Iulia Secunda

Fragment d’une stèle de marbre blanc, endommagée en haut et dans les angles inférieurs gauche et droit. Il ne subsiste que la dernière ligne du texte qui devait être délimité sur les côtés par des queues d’aronde. 1 

Roseau ou Cadat En provenance de la propriété André Roseau, dans le secteur de la nécropole occidentale.  Conservé au Musée de Cherchell (n° inv I. 12.).

Dimensions : "h." : 25 cm ; "l." :  cm ; "ep." : 8 cm ; "d." : 0 cm.

Texte d’au moins une ligne. H. d. l. : 1,5 cm (2,5 selon AE).  Dimensions : h. : 0 cm ; l. : 0 cm.

BAA VII, p. 121, n° 147 (AE 1985, 930).

h. d. l. : 1,5 - 1,5 cm

L’inscription devait être gravée dans un champ épigraphique en forme de tabula ansata, comme de nombreuses autres inscriptions de Cherchell (comme CIL VIII 21071, 21275, ou encore 21345). Aucune ligne incisée ne vient délimiter le champ épigraphique et la niche cintrée où figure le bas-relief (comme CIL VIII 10938 ou encore 21047).


------IVLIA SECVNDA

… Iulia Secunda.

Iconographie :  Fragment d'une stèle. Sur la partie supérieure figure le nom de Iulia Secunda gravé entre deux amorces de tabula ansata. Leur présence suggère qu'il s'agit d'une stèle à fronton triangulaire qui a été retaillée. En dessous dans un second  registre, une fillette est figurée en relief dans une niche voûtée.  Elle a un visage rond aux traits fins. Ses cheveux sont ondulés et séparés par une tresse médiane plate, semblable à celle de Flora (CIL VIII, 9473). Elle est vêtue d’une tunique talaire et d’un manteau qui ne couvre que l’épaule gauche. Elle tient avec la main droite, le bras tendu, une grappe de raisin et avec la main gauche, le bras ramené vers sa poitrine, une pomme. Ce type de stèle et la briéveté de l'épitaphe suggèrent une datation de l'époque royale.

Datation : Première moitié du 1er siècle de notre ère.

Il manque le haut de l'inscription. La brièveté de l'épitaphe, le rapprochement iconographique avec la stèle de Flora (https://doi.org/10.34927/ccj.epi.329) ainsi que la coiffure julio-claudienne datent la stèle de la première moitié du Ier siècle apr. J.-C.. Épigraphie : Malgré la perte de la partie supérieure du monument – très certainement d’une ligne d’après le motif en queue d’aronde –, l’épitaphe devait être très brève. La présence du gentilice impérial Iulius (J.-M. Lassère, Ant.Afr. 1973, p. 54), ainsi que la brièveté de l’inscription pousse à dater le texte du Ier siècle.   

