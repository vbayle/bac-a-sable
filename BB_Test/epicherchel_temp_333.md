# Métadonnées du document

1. DC Title : Épitaphe versifiée de l’épouse de Blandus
2. DC Creator : 
3. DC Subject : funéraire
4. DC Description : Plaque fragmentaire (CIL) ou stèle à fronton (A. SCHMITTER, 1883, p. 47 n° 60 et p. 246), brisée à droite et en bas, dont il ne reste que la moitié gauche. Dimensions inconnues.
5. DC Publisher : Epicherchel Corpus des inscriptions antiques de Cherchel - Centre Camille Jullian
6. DC Contributor : étude Christine Hamdoune
7. DC Date : Début du Ier siècle de notre ère
8. DC Type : plaque
9. DC Format : Markdown
10. DC Identifier : CIC-333
11. DC Source : http://ccj-epicherchel.huma-num.fr/interface/phototheque/333/21146.png
12. DC Language : la
13. DC Relation : 
14. DC Coverage : Cherchell ; Tipaza ; Algerie ; Iol Caesarea
15. DC Rights : Licensed under a Creative Commons CC BY-NC-ND 4.0. ; All reuse or distribution of this work must contain somewhere a reference to http://ccj-epicherchel.huma-num.fr/fr/le-projet-epicherchel/ ; Sauf mention contraire les photographies appartiennent au Centre Camille Jullian (Aix Marseille Univ, CNRS, Minist Culture  Com)

# Métadonnées du document

## edition

CONIVNX ▴ HIC ▴ BLANDI ▴ S[---]   QVEM ▴ FORTVNA ▴ PRECO[---]VLTVMA ▴ TER ▴ DENOS ▴ AC ▴ TR[---]   CARA ▴ VIRO ▴ VIXI ▴ MORIA[---]VNVM ▴ EQVIDEM ▴ DOLEO ▴ LIQV[---]TAM   IN SOCIA ▴ VIRO ▴ SPEM ▴ MEA[---][-]RECOR ▴ O ▴ [.]ONIVNX ▴ NOSTR[---]   [---]SSIDVE ▴ CELEBR[---]

## apparatus

À la ligne 7, F. Bücheler propose insocia(ta) employé pour insociabilis (dans le Thesaurus Linguae Latinae, une attestation luminaria insociata dans S. FERABOLI, S. MATTON, 1994, 20, 31 S.).

## translation

C’est ici que je repose, ---, épouse de Blandus que je prie la Fortune de ---. Je n’ai pas dépassé --- trente-trois ans, j’ai vécu chère à mon époux, de sorte que je meurs ---. Pour ma part, je déplore une chose : j’ai quitté la vie sans plus de lien avec mon mari ---, ma [mort en a emporté] l’espoir. Je te supplie, ô mon époux, ---, ne cesse pas d’honorer notre ---.

## bibliography

A. SCHMITTER, III, 1883, p. 47 n° 60 et p. 246 (CLE II 1290 ; CIL VIII 21146) ; Chr. HAMDOUNE, 2011, p. 260-261 n° 161

Chr. HAMDOUNE, 2013, p. 5-17. 

