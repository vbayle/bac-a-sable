---
title : "35 - Épitaphe de Flavia Apama"
author : "étude Philippe Leveau"
date : ""
publisher : "Epicherchell Corpus des inscriptions antiques de Cherchell - Centre Camille Jullian"
rights : "Licensed under a Creative Commons CC BY-NC-ND 4.0."
---

# 35 - Épitaphe de Flavia Apama

AJOUT DE TEXTE PAR BB

Plaque en marbre blanc brisée à droite. 1 

indéterminé Trouvée dans la nécropole occidentale, près du rempart.  Conservé au Musée de Cherchell (n° inv ).

Dimensions : "h." : 0 cm ; "l." : 24 cm ; "ep." : 4 cm ; "d." : 0 cm.

 Dimensions : h. : 0 cm ; l. : 0 cm.

BAA VII, p. 118, n° 135 ; AE 1985, 920

Points de séparation triangulaires. Ligne 1, ligature AN. h. d. l. : 1,8 - 2 cm


DIS MAN[---]
FLA ▴ APAMA[---]
VIX ANNI[.] L[---]
C ▴ ANN IVSO[---]
SOCRAE SVAE
BENE ▴ MERENT[---]
FECIT
S T T L

Aux Dieux Mânes. Flav(ia) Apama a vécu 50 ans. Caius Annius O(ptatus ?) ) sa belle-mère pour son bon mérite a fait. Que la terre te soit légère !

Datation : IIe—IIIe

