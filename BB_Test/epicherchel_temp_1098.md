# Métadonnées du document

1. DC Title : Epitaphe chrétienne du germain Fridila
2. DC Creator : 
3. DC Subject : chrétienne
4. DC Description : Table de marbre rectangulaire.
5. DC Publisher : Epicherchel Corpus des inscriptions antiques de Cherchel - Centre Camille Jullian
6. DC Contributor : Etude Bruno Pottier
7. DC Date : Seconde moitié du Ve siècle-début du VIe siècle
8. DC Type : plaque
9. DC Format : Markdown
10. DC Identifier : CIC-1098
11. DC Source : http://ccj-epicherchel.huma-num.fr/interface/phototheque/1098/109154_det.jpg http://ccj-epicherchel.huma-num.fr/interface/phototheque/1098/Galerie/109154.jpg
12. DC Language : la
13. DC Relation : 
14. DC Coverage : Cherchell ; Tipaza ; Algerie ; Iol Caesarea
15. DC Rights : Licensed under a Creative Commons CC BY-NC-ND 4.0. ; All reuse or distribution of this work must contain somewhere a reference to http://ccj-epicherchel.huma-num.fr/fr/le-projet-epicherchel/ ; Sauf mention contraire les photographies appartiennent au Centre Camille Jullian (Aix Marseille Univ, CNRS, Minist Culture  Com)

# Métadonnées du document

## edition

BMRFRIDILAINPAQVIESCITα⳨

## apparatus

## translation

A la bonne mémoire de Fridila qui repose en paix.

## bibliography

CIL VIII 21424 d'après A. Schmitter, Bull. épigr. de la Gaule, 1884, p. 63, n° 135 

ILCV 3108D ; C. Courtois, Les Vandales et l’Afrique, Paris, 1955, p. 387

