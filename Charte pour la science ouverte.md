# Charte pour la science ouverte
## Ouvrir les données

### Ouvrir les données de la recherche (Open Data)

### Ouvrir les codes sources (Open Source)

### Ouvrir les données patrimoniales (Open Content)

## Ouvrir les images

## Ouvrir l’accès aux publications scientifiques

### L’accès ouvert (Open Access)

### L’identité numérique des chercheuses et des chercheurs

## Ouvrir les méthodes et outils

### Codes

### Git institutionnel (MMSH)

## Stratégie de gestion des données

## Stratégie de la création de sites web

### Accessibilité numérique
cf. plaquette (HAL) issue du GT Accessibilité du réseau Médici (sous la dir. de V. Mansard)

cf. Berbache Rachid et Géroudet Madeleine (dirs.), Guide sur l’accessibilité numérique à destination des porteurs de revues scientifiques, Lille : Université de Lille, 2023, 11 p.

### Sobriété numérique : intégrer aux principes cette notion : principes FAIRS2

### Expérimenter et utiliser des chaînes low-tech cf. Chiragan, catalogue Van Dyck-Louvre

## Sensibiliser et former les chercheuses et chercheurs

### MMSH et science ouverte

### **La science ouverte (Open Science) **

La MMSH adhère aux principes promus par la « science ouverte » (Open Science), en accord avec le Plan national pour la science ouverte (PNSO) : rendre la recherche scientifique et les données produites accessibles à toutes et à tous, qu’il s’agisse des publications scientifiques, des données de la recherche ou des méthodes et des outils de travail.

## La charte science ouverte de la MMSH

La mise en place d’une stratégie pour la science ouverte est soutenu et coordonnée par la direction de la MMSH, qui délégue à un comité composé de personnes référentes pour l'ensemble du périmètre de la MMSH la mission de rédiger une charte et de l’inscrire dans le schéma directeur des systèmes d’information et du numérique (SDSI) de l’établissement (Implication du SI de la MMSH).

Cette charte, validée par un comité de relecture inter-service (MMSH, cellule SO AMU)  et adoptée par le conseil scientifique de l’Institut, sera révisée et mise à jour périodiquement, en adéquation avec les transformations du monde numérique et les évolutions des plans nationaux et internationaux pour la science ouverte.

L’institution d’un comité de pilotage pour la science ouverte est envisagée par la MMSH afin de suivre au mieux les politiques concernant l’ouverture de ses données de la recherche et l'accompagnement des projets (MMSH, SCD, CEDRE).

## Ouvrir les publications

### L’identité numérique des chercheuses et des chercheurs

La MMSH encourage une gestion responsable de l’identité numérique des chercheuses et des chercheurs en proposant des formations ciblées et en incitant à l’utilisation d’identifiants pérennes internationaux tels que l’identifiant ORCID.

La MMSH s’est par ailleurs engagé dans une politique claire et cohérente de définition d’identifiants uniques d’établissement (ROR, RNSR, Grid, ISNI, HAL, WikiData, IDREF, VIAF) afin de fournir des repères clairement identifiables et citables aux chercheuses et aux chercheurs de l’Institut.

en fin de charte :

## Participer aux appels à projet promouvant la science ouverte

L’INHA poursuit autant que faire se peut son engagement en faveur de la science ouverte, en participant aux appels à projet visant à promouvoir l’ouverture de ses ressources.

La bibliothèque de l’INHA est associée au GIS « CollEx-Persée », et l’Institut a proposé plusieurs projets lors des appels « CollEx-Persée », dont certains ont été lauréats (notamment CairMod, https://www.collexpersee.eu/projet/cairmod/).

En 2022, l’INHA a répondu au troisième appel du Fond national pour la science ouverte (FNSO), en proposant un projet qui explore des nouveaux formats éditoriaux et qui met l’image au coeur de la démonstration scientifique en utilisant les potentialités des technologies IIIF pour la mise à disposition de corpus visuels et des métadonnées associées.

L’INHA s’engage à poursuivre la participation aux appels à projet et à financement qui permettent de mettre en place des actions d’ouverture des ressources scientifiques produites par l’établissement en incitant ses chercheuses et ses chercheurs à s’impliquer dans cette démarche.

